#+TITLE: Short guide to Org formatting
#+LATEX_CLASS: koma-article
#+LATEX_CLASS_OPTIONS: [parskip=half]
#+LATEX_HEADER: \usepackage[osf, ttscale=.8]{libertine}
A quick overview of various formatting features
* Emphasis
=*bold*= \to *bold*; 
=/emph/= \to /emph/; and 
~=verb=~ \to =verb=

* List 
Numbered list:
: 1.  i1
: 2.  i2

Unnumbered list
: -  i1
: -  i2

Description list 
: - key1 :: value1
: - key2 :: value2

* "Blocks" 
** Headline
: * level 1 headline
: :PROPERTIES:
: :CUSTOM_ID: short-name
: :END:
: ** level 2 headline 
: see also section [[#short-name]]

** Images
: #+CAPTION: fancy caption with \alpha
: #+ATTR_LATEX: :width .9\textwidth
: [[file:/link/to/picture.tikz]]

** Table
: #+NAME: tbl
: |---+---+---|
: | a | b | c |
: |---+---+---|
: | 1 | 2 | 3 |
: |---+---+---|
: see [[tbl]].

** A random block
 say abstract
: #+begin_quote
: read me!
: #+end_quote
become =\begin{quote} read me! \end{quote}=.  

All blocks like
: \begin{abstract}
: ...
: \END{abstract}
are also recognized.
* Math
math inline: 
: $\alpha\beta$ or \(\gamma\delta\) or \theta\in\Theta
Stuff like single Greek letters don't need to be in maht

Environment:
: \begin{equation}
: \label{eq:1}
: \int x^2 = \frac{1}{3}x^3
: \end{equation}
: See  [[eq:1]]

* Citations 
below ~key~ is a bibtex key which is prefixed with ~@~ to denotes it
bibtexness. 

** Short citations:
: @key12   -> \textcite{key12}
: (@key12) -> \parencite{key12}

** Long citations,
: [[TYPE: PRE; PRE-1 @KEY-1 POST-1;...; PRE-N @KEY-N POST-N; POST]]
Transformed to something like 
: \TYPE(PRE)(POST)[PRE-1][POS-1]{KEY-1}...[PRE-N][POS-N]{KEY-N}
Which in turns look something like this
: (PRE PRE-1 AUTHOR-1, YEAR-1 POST-1, ...  and PRE-N AUTHOR-N, YEAR-N POST-N POST)

TYPE \in {~cite/textcite parentcite citeauthor citeyear~}.

* footnotes 
Either inline or remote footnotes:
:  An inline footnote[fn:key:mytext].  See also footnote number [[key]] and [[fn:1]].
: A remote footnote[fn:1].
:
:  [fn:1] can be anywhere in the document.